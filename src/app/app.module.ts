import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BibliotecaComponent } from './view/components/biblioteca/biblioteca.component';
import { InicioComponent } from './view/components/inicio/inicio.component';
import { LinksComponent } from './view/components/links/links.component';
import { LoginComponent } from './view/components/login/login.component';
import { SocialfeedsComponent } from './view/components/socialfeeds/socialfeeds.component';
import { TareasComponent } from './view/components/tareas/tareas.component';
import { FooterComponent } from './view/layout/footer/footer.component';
import { NavComponent } from './view/layout/nav/nav.component';
import { SidenavRightComponent } from './view/layout/sidenav-right/sidenav-right.component';
import { SidenavComponent } from './view/layout/sidenav/sidenav.component';

@NgModule({
  declarations: [
    AppComponent,
    BibliotecaComponent,
    InicioComponent,
    LinksComponent,
    LoginComponent,
    SocialfeedsComponent,
    TareasComponent,
    FooterComponent,
    NavComponent,
    SidenavRightComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [SidenavComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
