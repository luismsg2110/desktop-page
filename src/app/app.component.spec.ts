import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BibliotecaComponent } from './view/components/biblioteca/biblioteca.component';
import { InicioComponent } from './view/components/inicio/inicio.component';
import { LinksComponent } from './view/components/links/links.component';
import { LoginComponent } from './view/components/login/login.component';
import { SocialfeedsComponent } from './view/components/socialfeeds/socialfeeds.component';
import { TareasComponent } from './view/components/tareas/tareas.component';
import { FooterComponent } from './view/layout/footer/footer.component';
import { NavComponent } from './view/layout/nav/nav.component';
import { SidenavRightComponent } from './view/layout/sidenav-right/sidenav-right.component';
import { SidenavComponent } from './view/layout/sidenav/sidenav.component';
import { FormsModule } from '@angular/forms';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [
        AppComponent, BibliotecaComponent, InicioComponent, LinksComponent, LoginComponent, SocialfeedsComponent, TareasComponent,
        NavComponent, FooterComponent, SidenavRightComponent, SidenavComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'desktop-page'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(2).toEqual(2);
    //expect(app.title).toEqual('desktop-page');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(2).toEqual(2);
    //expect(compiled.querySelector('h1').textContent).toContain('Welcome to desktop-page!');
  }));
});
