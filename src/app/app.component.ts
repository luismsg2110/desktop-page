import { Component } from '@angular/core';
import * as info from './../app/services/data';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = info.data.app.app_name;
	navs = info.data.nav;
	username = info.data.app.user_name;
    cepBtn = info.data.app.ceptinelBtn;
	constructor() { }
}
