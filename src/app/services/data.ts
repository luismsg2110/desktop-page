export const data =
{
    "app":{
        "app_name":"Welcome to the <b>Ceptinel space for Compliance</b>",
        "copy":"RegTech Suite",
        "user_name":"Admin user",
        "ceptinelBtn":"Go to CEPTINEL"
    },
    "task":{
        "name":"My tasks",
        "btn_add":"Add a task",
        "modal_ttl":"Add a task",
        "label_ttl":"Title",
        "label_priority":"Priority",
        "label_option0":"Select an option",
        "label_option1":"high",
        "label_option2":"Normal",
        "label_option3":"low",
        "label_date":"Deadline",
        "label_descrip":"Description",
        "modal_accept":"Accept",
        "modal_cancel":"Cancel",
        "list":[
            {"color":"green",
                "name":"Publish SVS document 1",
                "label":"Complete",
                "checked":true
            },
            {"color":"red",
                "name":"Publish SVS document 2",
                "label":"To do",
                "checked":false
            },
            {"color":"green",
                "name":"Publish SVS document 3",
                "label":"Complete",
                "checked":true
            },
            {"color":"red",
                "name":"Publish SVS document 4",
                "label":"To do",
                "checked":false
            }
        ]
    },
    "social":{
        "name":"Social Feeds",
        "rrss":{
            "twitter":[
                {
                    "tittle":"post twitter title",
                    "text":"lorem twitter...",
                    "img":"banner/twitter.jpg"
                }
            ],
            "facebook":[
                {
                    "tittle":"post facebook title",
                    "text":"lorem facebook...",
                    "img":"banner/facebook.jpg"
                }
            ],
            "instagram":[
                {
                    "tittle":"post instagram title",
                    "text":"lorem instagram...",
                    "img":"banner/instagram.jpg"
                }
            ]
        }
    },
    "library":{
        "name":"Normative Library",
        "modal_ttl":"Normative Library",
        "modal_label_edit":"Edit",
        "modal_label_ttl":"Title",
        "modal_label_descrip":"Description",
        "modal_label_file":"Select a file",
        "modal_btn_accept":"Accept",
        "modal_btn_cancel":"Cancel",
        "list":[
            {
                "name":"UAF-Law 1321654",
                "descripcion":"UAF-Law lorem....",
                "files":[
                    {"name":"UAF-Law-19190.pdf","date":"24/08/2018"},
                    {"name":"UAF-Law-19190.pdf","date":"24/08/2018"},
                    {"name":"UAF-Law-19190.pdf","date":"24/08/2018"}
                ]
            },
            {
                "name":"UAF-Law 9821561",
                "descripcion":"UAF-Law lorem....",
                "files":[
                    {"name":"UAF-Law-19190.pdf","date":"24/08/2018"}
                ]
            },
            {
                "name":"UAF-Law 5894561",
                "descripcion":"UAF-Law lorem....",
                "files":[
                    {"name":"UAF-Law-19190.pdf","date":"24/08/2018"}
                ]
            },
            {
                "name":"UAF-Law 8971231",
                "descripcion":"UAF-Law lorem....",
                "files":[
                    {"name":"UAF-Law-19190.pdf","date":"24/08/2018"}
                ]
            }
        ]
    },
    "links":{
        "name":"Add link",
        "modal_ttl":"Add access to my space",
        "modal_label_name":"Name",
        "modal_label_url":"URL",
        "modal_btn_accept":"Accept",
        "modal_btn_cancel":"Cancel",
        "list":[
            {
                "name":"Santiago Stock Exchange",
                "link":"whttp://www.bolsadesantiago.com/Paginas/home.aspx"
            },
            {
                "name":"El mercurio inversiones",
                "link":"http://www.elmercurio.com/inversiones/"
            },
            {
                "name":"News ",
                "link":"https://www.cnnchile.com"
            }
        ]
    },
    "nav":[
        {
            "name": "Integrity and transparency market",
            "icon": "coin-1",
            "link": "#",
            "banner":[
                {
                    "img":"banner-1-0",
                    "text":"The system provides an Alert Monitor"
                },
                {
                    "img":"banner-1-1",
                    "text":"It informs in real time events that put your business at risk and directly threaten market transparency. "
                },
                {
                    "img":"banner-1-2",
                    "text":"You can visualize in detail those events that risk your business."
                },
                {
                    "img":"banner-1-3",
                    "text":"Provides a library of rules that support the work of the Compliance and Market Tracking area"
                },
                {
                    "img":"banner-1-4",
                    "text":"Monitoring and monitoring of cases with evidence of Market Abuse"
                },
                {
                    "img":"banner-1-5",
                    "text":"Tracking Graph of market indicators Chilean Stock"
                }
            ]
        },
        {
            "name": "GRC Framework and Regulatory Intelligence",
            "icon": "atomic",
            "link": "#",
            "banner":[]
        },
        {
            "name": "Financial crimes",
            "icon": "loupe",
            "link": "#",
            "banner":[]
        },
        {
            "name": "Regulatory reports analysis and calculations of regulatory records",
            "icon": "edit",
            "link": "#",
            "banner":[]
        },
        {
            "name": "Compliance - Insurance",
            "icon": "sprout",
            "link": "#",
            "banner":[]
        },
        {
            "name": "Information management and regulatory data ML",
            "icon": "line-chart-2",
            "link": "#",
            "banner":[]
        }]
}
