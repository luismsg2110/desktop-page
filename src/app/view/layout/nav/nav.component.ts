import { Component, OnInit } from '@angular/core';
import * as info from './../../../services/data';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  username = info.data.app.user_name;
  cepBtn = info.data.app.ceptinelBtn;
  
  constructor() { }

  ngOnInit() {
  }

}
