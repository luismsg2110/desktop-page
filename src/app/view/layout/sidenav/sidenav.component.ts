import { Component, OnInit } from '@angular/core';
import * as info from './../../../services/data';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent  {
  navs = info.data.nav;
  copy = info.data.app.copy;
  banner:number = 0;

  constructor() { }

  loadBanner(id){
  	this.banner = id;
  }

}
