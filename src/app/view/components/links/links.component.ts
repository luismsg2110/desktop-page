import { Component, OnInit } from '@angular/core';
import * as info from './../../../services/data';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css']
})
export class LinksComponent implements OnInit {
  links = info.data.links;
  constructor() { }

  ngOnInit() {
  }

}
