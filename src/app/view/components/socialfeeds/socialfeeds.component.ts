import { Component, OnInit } from '@angular/core';
import * as info from './../../../services/data';

@Component({
  selector: 'app-socialfeeds',
  templateUrl: './socialfeeds.component.html',
  styleUrls: ['./socialfeeds.component.css']
})
export class SocialfeedsComponent implements OnInit {
  social = info.data.social;
  redes = Object.keys(info.data.social.rrss);
  constructor() { }

  ngOnInit() {
  }

}
