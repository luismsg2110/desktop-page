import { Component, OnInit } from '@angular/core';
import * as info from './../../../services/data';

@Component({
  selector: 'app-biblioteca',
  templateUrl: './biblioteca.component.html',
  styleUrls: ['./biblioteca.component.css']
})
export class BibliotecaComponent implements OnInit {
  biblio = info.data.library;
  constructor() { }

  ngOnInit() {
  }

}
