import { Component, OnInit } from '@angular/core';
import * as info from './../../../services/data';
import * as $ from "jquery";

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  banner = info.data.nav[0].banner;

  constructor() {
  	
   }

  ngOnInit() {

  }

}
