$(document).ready(function(){
  
    setTimeout(function() {
        $('body').removeClass('isloading');
        Materialize.toast('App is ready', 4000,'green') 
    }, 500);

    $('.dropdown-button').dropdown({
          inDuration: 300,
          outDuration: 225,
          constrainWidth: true, // Does not change width of dropdown to that of the activator
          hover: false, // Activate on hover
          gutter: 15, // Spacing from edge
          belowOrigin: true, // Displays dropdown below the button
          alignment: 'left', // Displays dropdown with edge aligned to the left of button
          stopPropagation: false // Stops event propagation
      });

      $('ul.socialfeeds').tabs();

    $('.owl-carousel').owlCarousel({
          loop:true,
          margin:10,
          nav:false,
        items:1,
        autoplay: 3000,
        center:true,
        mouseDrag:false,
        touchDrag:false,
        navigation : true
      });

    $('.tooltipped').tooltip();
    $('.modal').modal({
        endingTop:'1%'
    });    $('select').material_select();
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false, // Close upon selecting a date,
        container: undefined, // ex. 'body' will append picker to body
      });

    $('[data-activates="slide-main"]').sideNav({
        menuWidth: 250,
        edge: 'left',
        closeOnClick: false,
        draggable: true,
        onOpen: function(el) {
            $('body').removeAttr('style');
            $('body').removeClass('sidenav_hide');
            if ($(window).width() >= 993){    
              $('#sidenav-overlay').addClass('hide');
              $('.drag-target').css('pointer-events','none');
            }
        },
        onClose: function(el) {
            $('body').removeAttr('style');
            $('body').addClass('sidenav_hide');
            if ($(window).width() >= 993){    
              $('#sidenav-overlay').removeClass('hide');
              $('.drag-target').css('pointer-events','inherit');
            }
        },
    });

    if ($(window).width() >= 993){    
        $('[data-activates="slide-main"]').sideNav('show');
        $('#slide-main').addClass('nav-active');
    }

    $('[data-activates="slide-right"]').sideNav({
        menuWidth: 245,
        edge: 'right',
        closeOnClick: true,
        draggable: true,
        onOpen: function(el) {},
        onClose: function(el) {},
    });

    $(window).resize(function(){     
        if ($(window).width() <= 993){    
            $('[data-activates="slide-main"]').sideNav('hide');
            $('#slide-main').removeClass('nav-active');
        }else{
            if(!$('#slide-main').hasClass('nav-active')){
                $('[data-activates="slide-main"]').sideNav('show');
                $('#slide-main').addClass('nav-active');
            }
        }
    });
    codeview()
});//end ready

function codeview(){
  $('.codeview').each(function (index, value) { 
    let lehtml = $('.codeview').prop('outerHTML');
    let str1 = lehtml.replace(/</g, '&lt;');
    let str2 = str1.replace(/>/g, '&gt;');
    let template = `<pre class="grey lighten-2 grey-text text-darken-3"><code>          ${str2}</code></pre>`;
    $(template).insertAfter(this);
  });
}

function confirmaction(){
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn white-text green',
      cancelButtonClass: 'btn white-text orange',
      buttonsStyling: false,
    })

    swalWithBootstrapButtons({
      title: '¿Esta seguro?',
      text: "los cambios no podran ser revertidos",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Borralo',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons(
          'Borrado!',
          'Tu archivo ha sido eliminado',
          'success'
        )
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons(
          'Cacelado',
          'Nada ha cambiado',
          'error'
        )
      }
    })
}